package com.example.tablasdemultiplacar;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TableX extends Activity implements View.OnClickListener{

    private TextView tvresultado,txt1;
    private Button bt1, bt2, bt3, bt4, bt5, bt6,bt7,bt8,bt9,bt10,btborrar;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.principal);

        mapUI();
    }

    private void mapUI(){
        tvresultado= (TextView) findViewById(R.id.tvResultado);
        txt1=(TextView) findViewById(R.id.txt1);
        bt1=(Button) findViewById(R.id.bt1);
        bt2=(Button) findViewById(R.id.bt2);
        bt3=(Button) findViewById(R.id.bt3);
        bt4=(Button) findViewById(R.id.bt4);
        bt5=(Button) findViewById(R.id.bt5);
        bt6=(Button) findViewById(R.id.bt6);
        bt7=(Button) findViewById(R.id.bt7);
        bt8=(Button) findViewById(R.id.bt8);
        bt9=(Button) findViewById(R.id.bt9);
        bt10=(Button) findViewById(R.id.bt10);
        btborrar=(Button)findViewById(R.id.btBorrar);
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        bt4.setOnClickListener(this);
        bt5.setOnClickListener(this);
        bt6.setOnClickListener(this);
        bt7.setOnClickListener(this);
        bt8.setOnClickListener(this);
        bt9.setOnClickListener(this);
        bt10.setOnClickListener(this);
        btborrar.setOnClickListener(this);
    }


    public void onClick(View v) {
        int id= v.getId();
        int i = 0;
        if(id!=btborrar.getId()) {
            tvresultado.setText("");
            if (id == bt1.getId()) {
                txt1.setText(bt1.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt2.getId()) {
                txt1.setText(bt2.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt3.getId()) {
                txt1.setText(bt3.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt4.getId()) {
                txt1.setText(bt4.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt5.getId()) {
                txt1.setText(bt5.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt6.getId()) {
                txt1.setText(bt6.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt7.getId()) {
                txt1.setText(bt7.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt8.getId()) {
                txt1.setText(bt8.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt9.getId()) {
                txt1.setText(bt9.getText());
                i = Integer.parseInt(txt1.getText().toString());
            } else if (id == bt10.getId()) {
                txt1.setText(bt10.getText());
                i = Integer.parseInt(txt1.getText().toString());
            }

            for (int j=1;j<=10;j++){
                tvresultado.setText(tvresultado.getText()+"\n"+i+" x "+j+" = "+i * j);
            }
        } else{
            tvresultado.setText("");
            txt1.setText("");
        }
    }
}
